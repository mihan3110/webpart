package servs;

import stringutils.StringUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MainServlet extends HttpServlet {
//http://localhost:8080/test/reverse?string=123
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {


        String string= req.getParameter("string");
        PrintWriter out = resp.getWriter();
        out.print( StringUtil.reverseString(string));

    }



}